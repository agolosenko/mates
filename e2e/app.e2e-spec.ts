import { MegaplanPage } from './app.po';

describe('megaplan App', function() {
  let page: MegaplanPage;

  beforeEach(() => {
    page = new MegaplanPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
