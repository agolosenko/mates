import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MatesListComponent } from './components/mates-list/mates-list.component';
import { MatesFormComponent } from './components/mates-form/mates-form.component';

import { JsonImportService } from './services/json-import.service';
import {MateRepositoryService} from "./services/mate-repository.service";

@NgModule({
  declarations: [
    AppComponent,
    MatesListComponent,
    MatesFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    JsonImportService,
    MateRepositoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
