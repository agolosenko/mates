import { Component, OnInit } from '@angular/core';
import { MateRepositoryService } from '../../services/mate-repository.service';
import {Mate} from "../../entities/mate";

@Component({
  selector: 'app-mates-list',
  templateUrl: './mates-list.component.html',
  styleUrls: ['./mates-list.component.css']
})
export class MatesListComponent implements OnInit {
    private editingMate: Mate;
    private creatingMate: Mate;
    constructor(private repository: MateRepositoryService) { }

    ngOnInit() {

    }

    createMate() {
        this.creatingMate = this.repository.create();
    }

    editMate(mate) {
        this.editingMate = mate;
    }

    deleteMate(guid: string) {
        this.repository.remove(guid);
    }

    onFormHide() {
        delete this.editingMate;
        delete this.creatingMate;
    }

}
