import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Mate} from "../../entities/mate";
import { MateRepositoryService } from '../../services/mate-repository.service';

@Component({
  selector: 'app-mates-form',
  templateUrl: './mates-form.component.html',
  styleUrls: ['./mates-form.component.css']
})
export class MatesFormComponent implements OnInit {
    @Input() mate: Mate;
    @Output() onFormHide = new EventEmitter<boolean>();

    constructor(private repository: MateRepositoryService) { }

    ngOnInit() {
    }

    cancel() {
        this.onFormHide.emit();
    }

    submit(form) {
        this.applyToMate(this.mate, form)
        this.repository.update(this.mate);
        this.onFormHide.emit();
    }

    private applyToMate(mate, form) {
        mate.name.first = form.value.firstName;
        mate.name.last = form.value.lastName;
        mate.age = form.value.age;
        mate.email = form.value.email;
    }

}
