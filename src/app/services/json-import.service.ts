import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Mate} from "../entities/mate";

@Injectable()
export class JsonImportService {

  constructor(private http:Http) { }
  private get(name: string) {
    return this.http.request('/assets/data/' + name + '.json');
  }

  public getMates(context, variable) {
    return this.get('mates').map(res => <Mate[]>res.json())
        .subscribe(data => context[variable]= data,
            err => console.log(err),
            () => console.log('Completed'));
  }
}
