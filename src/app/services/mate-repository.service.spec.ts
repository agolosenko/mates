/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MateRepositoryService } from './mate-repository.service';

describe('Service: MateRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MateRepositoryService]
    });
  });

  it('should ...', inject([MateRepositoryService], (service: MateRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
