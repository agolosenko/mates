import { Injectable } from '@angular/core';
import { JsonImportService } from './json-import.service';
import {Mate} from "../entities/mate";
import * as uuid from 'uuid';
@Injectable()

export class MateRepositoryService {
    private mates: Mate[];
    constructor(private json: JsonImportService) {
        this.json.getMates(this, 'mates');
    }

    private getMateIndexByUuid(guid: string):number {
    var index = -1;
    this.mates.map(function(mate: Mate, i){
        if (mate.guid == guid) {
            index = i;
            return false;
        }
    });

    return index;
    }

    create(): Mate {
        return new Mate(uuid.v4());
    }

    /**
    * Returns list of mates
    * @returns {Mate[]}
     */
    getMates():Mate[] {
      return this.mates;
    }
    

    /**
    * Removes mate with such guid
    * @param guid
    * @returns {boolean}
     */
    remove(guid: string):boolean {
        var mateIndex = this.getMateIndexByUuid(guid);
        if (mateIndex !== -1) {
        this.mates.splice(mateIndex, 1);
            return true;
        }
        return false;
    }

    /**
    * @param guid
    * @param mutated
    * @returns {boolean}
     */
    update(mutated: Mate): void {
        var mateIndex = this.getMateIndexByUuid(mutated.guid);
        if (mateIndex !== -1) {
            this.mates[mateIndex] = mutated;
        } else {
            this.mates.push(mutated);
        }
    }

}
