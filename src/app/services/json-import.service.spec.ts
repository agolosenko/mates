/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JsonImportService } from './json-import.service';

describe('Service: JsonImport', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JsonImportService]
    });
  });

  it('should ...', inject([JsonImportService], (service: JsonImportService) => {
    expect(service).toBeTruthy();
  }));
});
