/**
 * Created by artem on 16.11.2016.
 */
export class Mate {
    constructor(
        public guid: string,
        public email?: string,
        public age?: string,
        public name?: string[]
    ) {
        if (name == null) {
            this.name = ['',''];
        }
    }

}