Необходимо разработать CRUD интерфейс для работы с базой сотрудников.

Обязательными являются следующие функции:

  - список людей
  - создание
  - редактирование
  - удаление
  - фильтрация списка (необязательно)

Приложение должно прочитать базу один раз из файла **mates.json**. Ее содержимое можно положить, например, в глобальную переменную **window.db**

Приложение должно быть чисто frontend. Базу сохранять обратно в файл не нужно.

При создании записи форма должна валидироваться.